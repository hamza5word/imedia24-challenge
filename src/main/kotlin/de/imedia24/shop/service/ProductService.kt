package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import org.springframework.stereotype.Service
import org.springframework.util.ReflectionUtils
import java.lang.reflect.Field
import java.math.BigDecimal
import java.util.stream.Collectors
import kotlin.reflect.jvm.internal.impl.load.kotlin.JvmType

@Service
class ProductService(private val productRepository: ProductRepository) {

    fun findProductBySku(sku: String): ProductResponse? {
        return productRepository.findBySku(sku)?.toProductResponse();
    }

    fun findProductsBySkuList(skus: String): List<ProductResponse>? {
        return productRepository.findBySkuIn(skus.split(","))?.stream()?.map { p -> p.toProductResponse() }?.collect(Collectors.toList());
    }

    fun addProduct(product: ProductRequest): ProductResponse? {
        return productRepository.save(product.toProductEntity()).toProductResponse();
    }

    fun updateProduct(sku: String, fields: Map<String, Any>): ProductResponse? {
        val product: ProductEntity? = productRepository.findBySku(sku)
        if(product != null) {
            updateProductFieldsFromMap(product, fields)
            return productRepository.save(product).toProductResponse()
        }
        return null
    }

    fun updateProductFieldsFromMap(product: ProductEntity, fields: Map<String, Any>) {
        fields.forEach { (key, value) ->
            val field = ReflectionUtils.findField(product.javaClass, key)
            field?.isAccessible = true
            if (field != null) {
                if(field.name.equals("price") && value is Int)
                    ReflectionUtils.setField(field, product, BigDecimal(value))
                else
                    ReflectionUtils.setField(field, product, value)
            }
            field?.isAccessible = false
        }
    }
}
