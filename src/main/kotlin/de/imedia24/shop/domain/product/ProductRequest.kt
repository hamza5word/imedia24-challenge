package de.imedia24.shop.domain.product

import de.imedia24.shop.db.entity.ProductEntity
import java.math.BigDecimal
import java.time.ZonedDateTime

data class ProductRequest(
    val sku: String?,
    val name: String?,
    val description: String?,
    val price: BigDecimal?
) {
    fun toProductEntity(): ProductEntity {
        return ProductEntity(
          sku?:"",
          name?:"",
          description,
          price?: BigDecimal(0),
          ZonedDateTime.now(),
          ZonedDateTime.now()
        )
    }
}
