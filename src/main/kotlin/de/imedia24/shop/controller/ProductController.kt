package de.imedia24.shop.controller

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.websocket.server.PathParam
import kotlin.reflect.jvm.internal.impl.load.kotlin.JvmType

@RequestMapping("products")
@RestController
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @GetMapping("{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku)
        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    @GetMapping(produces = ["application/json;charset=utf-8"])
    fun findProductsBySkuList(
        @RequestParam("skus") skus: String
    ) : ResponseEntity<List<ProductResponse>> {
        logger.info("Request for products $skus")

        val products = productService.findProductsBySkuList(skus);
        return if(products == null || products.isEmpty()) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(products)
        }
    }

    @PostMapping(consumes = ["application/json;charset=utf-8"], produces = ["application/json;charset=utf-8"])
    fun addProduct(
        @RequestBody productInput: ProductRequest
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product adding $productInput")

        val product = productService.addProduct(productInput);
        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    @PatchMapping("{sku}", consumes = ["application/json;charset=utf-8"], produces = ["application/json;charset=utf-8"])
    fun updateProduct(
        @PathVariable("sku") sku: String,
        @RequestBody fields: Map<String, Any>
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product updating $sku")

        val product = productService.updateProduct(sku, fields);
        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }
}
