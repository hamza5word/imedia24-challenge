CREATE TABLE products
(
    sku         VARCHAR(16)     NOT NULL
        CONSTRAINT pk_product_id PRIMARY KEY,
    name        VARCHAR(125)    NOT NULL,
    description VARCHAR(125),
    price       DECIMAL           NOT NULL,
    created_at  TIMESTAMP     NOT NULL,
    updated_at  TIMESTAMP     NOT NULL
);

INSERT INTO products VALUES ( '100', 'Computer Dell XPS', '', 1200, SYSDATE(), SYSDATE() );
INSERT INTO products VALUES ( '101', 'Computer Dell XPS', '', 1200, SYSDATE(), SYSDATE() );
INSERT INTO products VALUES ( '102', 'Computer Dell XPS', '', 1200, SYSDATE(), SYSDATE() );
INSERT INTO products VALUES ( '103', 'Computer Dell XPS', '', 1200, SYSDATE(), SYSDATE() );
INSERT INTO products VALUES ( '104', 'Computer Dell XPS', '', 1200, SYSDATE(), SYSDATE() );
INSERT INTO products VALUES ( '105', 'Computer Dell XPS', '', 1200, SYSDATE(), SYSDATE() );
INSERT INTO products VALUES ( '106', 'Computer Dell XPS', '', 1200, SYSDATE(), SYSDATE() );
INSERT INTO products VALUES ( '107', 'Computer Dell XPS', '', 1200, SYSDATE(), SYSDATE() );
INSERT INTO products VALUES ( '108', 'Computer Dell XPS', '', 1200, SYSDATE(), SYSDATE() );