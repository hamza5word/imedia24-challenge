package de.imedia24.shop

import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductResponse
import org.junit.Assert
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.getForEntity
import org.springframework.boot.test.web.client.patchForObject
import org.springframework.http.HttpStatus

@SpringBootTest(
	webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
	properties = [
		"spring.datasource.url=jdbc:h2:mem:testdb"
	]
)
class ShopApplicationTests(@Autowired val client: TestRestTemplate) {

	@Test
	fun contextLoads() {
	}


	@Test
	fun testGetProductBySku() {
		val response = client.getForEntity<ProductRequest>("/products/100")
		Assert.assertEquals(response.statusCode, HttpStatus.OK)
		Assert.assertEquals(response.body?.sku, "100")
	}

	@Test
	fun testGetProductBySkuList() {
		val response = client.getForEntity<List<ProductRequest>>("/products?skus=100,101,102")
		Assert.assertEquals(response.statusCode, HttpStatus.OK)
		Assert.assertTrue(response.body?.size!! > 0)
	}

	@Test
	fun testUpdateProduct() {
		val url = "/products/100"
		val body: Any = "body"
		val vars = mapOf(Pair("name", "TEST01"), Pair("price", 1600))
		val response = client.patchForObject<ProductResponse>(url, body, vars)
	}

}
